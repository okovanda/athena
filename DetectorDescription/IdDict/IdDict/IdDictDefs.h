/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IDDICT_IDDICTDEFS_H
#define IDDICT_IDDICTDEFS_H

#include "IdDict/IdDictFieldImplementation.h"
#include "IdDict/IdDictDictionary.h"
#include "IdDict/IdDictMgr.h"
#include "IdDict/IdDictField.h"
#include "IdDict/IdDictLabel.h"
#include "IdDict/IdDictDictEntry.h"
#include "IdDict/IdDictGroup.h"
#include "IdDict/IdDictAltRegions.h"
#include "IdDict/IdDictRegion.h"
#include "IdDict/IdDictRegionEntry.h"
#include "IdDict/IdDictSubRegion.h"
#include "IdDict/IdDictRange.h"
#include "IdDict/IdDictRangeRef.h"
#include "IdDict/IdDictReference.h"
#include "IdDict/IdDictDictionaryRef.h"
#include "Identifier/MultiRange.h"//to be removed later

#endif  
  

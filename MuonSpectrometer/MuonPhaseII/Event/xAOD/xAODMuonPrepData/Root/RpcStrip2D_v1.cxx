/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
// Local include(s):
#include "xAODMuonPrepData/versions/RpcStrip2D_v1.h"

namespace xAOD {
    uint8_t RpcStrip2D_v1::measuresPhi() const { return 0; }
}  // namespace xAOD

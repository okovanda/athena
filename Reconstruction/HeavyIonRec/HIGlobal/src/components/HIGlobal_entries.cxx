#include "HIGlobal/HIEventShapeMaker.h"
#include "HIGlobal/HIEventShapeFillerTool.h"
#include "../SpacePointCopier.h"
#include "../MBTSInfoCopier.h"
#include "../CaloClustersCopier.h"

DECLARE_COMPONENT( HIEventShapeMaker )
DECLARE_COMPONENT( HIEventShapeFillerTool )
DECLARE_COMPONENT( SpacePointCopier )
DECLARE_COMPONENT( MBTSInfoCopier )
DECLARE_COMPONENT( CaloClustersCopier )
